import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../app/HomeScreen/home/home.component';
import { AboutComponent } from '../app/HomeScreen/about/about.component';
import { PatchNotesComponent } from './Updates/patch-notes/patch-notes.component';
import { FirstPatchComponent } from './Updates/first-patch/first-patch.component';
import { SignInComponent } from './Login/sign-in/sign-in.component';
import { SignUpComponent } from './Login/sign-up/sign-up.component';
import { PreviewComponent } from './HomeScreen/preview/preview.component';
import { ErrorComponent } from './error/error.component';

const routes: Routes = [
  {path: '', redirectTo: '/Home', pathMatch: 'full'},
  {path: 'Home', component: HomeComponent},
  {path: 'AboutUs', component: AboutComponent},
  {path: 'SignIn', component:SignInComponent},
  {path: 'SignUp', component:SignUpComponent},
  {path: 'PatchNotes', component:PatchNotesComponent},
  {path: 'FirstPatch', component:FirstPatchComponent},
  {path: 'Preview', component:PreviewComponent},
  {path: '**', component: ErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
